# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 10:35:22 2021

@author: Bethan
"""
import math
#Kineamtics equations
x = springdisplacement
r1 = 48.545           
r = 35.155            
a = 132.418           
b = 5.396             
k = 67.8              
L0 = 93.044
L = L0 + x
F = k * x   #Force needed to be exerted by spring
lamda

d = thread distance from centre
e = horizontal distance from R to pivot
W = weight of leg
f = distance from W to pivot

c1 = (a*math.cos(k)-(b-r)*math.sin(k))/r1
c2 = ((a*math.sin(k)*math.sin(lamda))+(b-r)*(math.sin(lamda)*math.cos(k)))
R = (F*r/L*e)*((d*c1)+c2)-(W*f/e)




#MATLAB CODING
K=13.7
Fspring= 500 #force tob be exerted by spring
rTri=20   #radius of triangel pice attached to spring
rAS = 106.809958   #dist between angle axiss of rotation and spring connection
lo=rAS-rTri   #length of spring with minimum extension

def TriAngle(Fspring, K, lo, rTri, rAS):
    xSpring=Fspring/(K)  #does K need to be minus, was in MATLAB
    a=xSpring+lo
    b=rAS
    c=rTri
    TriAngle=math.acos(((a**2)-(b**2)-(c**2))/(-2*b*c))
    return TriAngle
    
#TriAngle ankle of triangle requied to stretc spring
#AnkleAngle angle of shank with ref to foot
#Ac=AngleForCorrection(TriAngle,AnkleAngle)

def AngleForCorrection(TriAngle,AnkleAngle):
    Ac=TriAngle-AnkleAngle
    return Ac

rTri2=30 #radius f triangel piece
lo2=198.062 - rTri2
lead=4 #lead of ball screw
GR=9/12 #gear ratio number of ballscrew/motor


def MotorDegreesOfRotation(TriAngle,lo2,rTri2,lead,GR,Ac):
    b=lo2+rTri2
    c=rTri2
    xBllSkrw=-lo2+math.sqrt((b**2)+(c**2)-(b*c*math.cos(Ac)))
    MDR=(360*xBllSKRW*GR)/lead
    return MDR






