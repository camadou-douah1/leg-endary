# leg-firmware/docs/datahandling/preprocessing.md

import numpy as np
from scipy import signal

def constrain_length(data, n):
	return data[len(data) - n:]

def append_data(data, additional_data):
	return np.append(data, additional_data, axis=0)

def lowpass_butter_filter(data, cut):
	cutoff = 1 / cut
	b, a = signal.butter(4, cutoff, btype='lowpass')
	filter_data = signal.filtfilt(b, a, data)
	return filter_data
