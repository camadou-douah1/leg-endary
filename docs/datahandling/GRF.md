# GRF.py
```bash
leg-firmware    
│
└───leg-firmware
│   │
│   └───datahandling
│       │   grf.py
│       │   ...
│    
...
```

## get_GRF(grf, gait_cycle, type)

### Description

Retrieves ground reaction force at given grf, gait cycle and walking type

### Parameters

**grf** = Ground reaction force type  
**gait_cycle** = 0 to 1 value for gait%  
**type** = Walking type  

### Parameter Options

**grf** (**string**) = "Anterior/Posterior" or "Medio/Lateral" or "Vertical"  
**gait_cycle** (**float**) = 0 to 1 step 0.01  
**type** (**string**) = "XS - 1" or "XS" or "XS + 1" or "S - 1" or "S" or "S + 1" or "M - 1" or "M" or "M + 1" or "L - 1" or "L" or "L + 1" or "Toe - 1" or "Toe" or "Toe + 1" or "Heel - 1" or "Heel" or "Heel + 1" or "Ascending - 1" or "Ascending" or "Ascending + 1" or "Decending - 1" or "Decending" or "Descending + 1"

### Returns
Requested ground reaction force (**float**) or None

### Usage Example

#### Input
```bash
get_GRF("Anterior/Posterior", 0.1, "XS")
```

#### Output
```bash
-0.0913496762514114
````