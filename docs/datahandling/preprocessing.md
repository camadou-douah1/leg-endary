# preprocessing.py
```bash
leg-firmware    
│
└───leg-firmware
│   │
│   └───datahandling
│       │   preprocessing.py
│       │   ...
│    
...
```
## constrain_length(data, n)

### Description

Limits the length of the given **data** to the final **n**

### Parameters

**data** = Data array to constrain  
**n** = Desired cardinality 

### Returns
Given **data** limited to **n** cardinality or None

### Usage Example

#### Input
```bash
constrain_length([2, 4, 6, 2, 9, 5], 3)
```
#### Output
```bash
[2, 9, 5]
````

## append_data(data, additional_data)

### Description

Concatenates **data** to **additional_data**

### Parameters

**data** = First data array  
**additional_data** = Second data array  

### Returns
Given **data** concatonated to **additional_data**

### Usage Example

#### Input
```bash
append_data([2, 4, 6], [3, 2, 9, 5])
```

#### Output
```bash
[2 4 6 3 2 9 5]
````

## lowpass_butter_filter(data, cut)

### Description

Lowpass butter filters the given **data** using given cutoff point

### Parameters

**data** = Data array to filter  
**cut** = Number to calculate cutoff point  

### Returns
Given **data** after being butter filtered

### Usage Example

#### Input
```bash
lowpass_butter_filter([2, 4, 6, 5, 1, 2, 4, 2, 1, 5, 9, 1, 4, 5, 3, 0], 3)
```

#### Output
```bash
[2.00337063 3.97672942 4.88608425 4.43694256 3.21509974 2.13604543 1.81383957 2.33100327 3.35318449 4.36077941 4.93953157 4.9845602 4.56971625 3.64976082 2.08761388 0.01003643]
````